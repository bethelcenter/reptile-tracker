This is a personal project to build an HTML and PHP front end for a MySQL database
to track the growth and care of personal reptiles. 

'Critters' Table:


*************************** 1. row ***************************
  Field: id
   Type: varchar(255)
   Null: NO
    Key: PRI
Default: NULL
  Extra: 
*************************** 2. row ***************************
  Field: record_date
   Type: varchar(255)
   Null: NO
    Key: 
Default: NULL
  Extra: 
*************************** 3. row ***************************
  Field: name
   Type: varchar(255)
   Null: NO
    Key: 
Default: NULL
  Extra: 
*************************** 4. row ***************************
  Field: sex
   Type: varchar(255)
   Null: NO
    Key: 
Default: UNK
  Extra: 
*************************** 5. row ***************************
  Field: weight
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 6. row ***************************
  Field: last_fed
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 7. row ***************************
  Field: size_of_food
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 8. row ***************************
  Field: water_change
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 9. row ***************************
  Field: substrate_cleaned
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 10. row ***************************
  Field: blueing_date
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 11. row ***************************
  Field: day_shed
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
*************************** 12. row ***************************
  Field: injuries
   Type: varchar(255)
   Null: YES
    Key: 
Default: NULL
  Extra: 
