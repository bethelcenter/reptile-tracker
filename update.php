<?php
// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$id = $record_date = $name = $sex = $weight = $last_fed = $size_of_food = $water_change = $substrate_cleaned = $blueing_date = $day_shed = $injuries = "";
$id_err = $record_date_err = $name_err = $sex_err = $weight_err = $last_fed_err = $size_of_food_err = $water_change_err = $substrate_cleaned_err = $blueing_date_err = "";
$day_shed_err = $injuries_err = "";

// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];

    // Validate Record Date
    $input_record_date = trim($_POST["record_date"]);
    if(empty($input_record_date)){
        $record_date_err = "Please enter a date.";
    } else {
        $record_date = $input_record_date;
    }

    // Validate name
    $input_name = trim($_POST["name"]);
    if(empty($input_name)){
        $name_err = "Please enter a name.";
    } elseif(!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Please enter a valid name.";
    } else{
        $name = $input_name;
    }

    $input_sex = trim($_POST["sex"]);
    if(empty($input_sex)){
        $sex = "UNK";
    } else{
        $sex = $input_sex;
    }

    // Validate Weight
    $input_weight = trim($_POST["weight"]);
    if(empty($input_weight)){
        $weight = NULL;
    } else{
        $weight = $input_weight;
    }

    // Validate Day Fed
    $input_last_fed = trim($_POST["last_fed"]);
    if(empty($input_last_fed)){
        $last_fed = NULL;
    } else{
        $last_fed = $input_last_fed;
    }

    // Validate Size of Food
    $input_size_of_food = trim($_POST["size_of_food"]);
    if(empty($input_size_of_food)){
        $size_of_food = NULL;
    } else{
        $size_of_food = $input_size_of_food;
    }


    $input_water_change = trim($_POST["water_change"]);
    if(empty($input_water_change)){
        $water_change = NULL;
    } else{
        $water_change = $input_water_change;
    }

    $input_substrate_cleaned = trim($_POST["substrate_cleaned"]);
    if(empty($input_substrate_cleaned)){
        $substrate_cleaned = NULL;
    } else{
        $substrate_cleaned = $input_substrate_cleaned;
    }

    $input_blueing_date = trim($_POST["blueing_date"]);
    if(empty($input_blueing_date)){
        $blueing_date = NULL;
    } else{
        $blueing_date = $input_blueing_date;
    }

    $input_day_shed = trim($_POST["day_shed"]);
    if(empty($input_day_shed)){
        $day_shed = NULL;
    } else{
        $day_shed = $input_day_shed;
    }

    $input_injuries = trim($_POST["injuries"]);
    if(empty($input_injuries)){
        $injuries = NULL;
    } else{
        $injuries = $input_injuries;
    }

    // Check input errors before inserting in database
    if(empty($record_date_err) && empty($name_err) && empty($sex_err) && empty($weight_err) && empty($last_fed_err)
        && empty($size_of_food_err) && empty($water_change_err) && empty($substrate_cleaned_err) && empty($blueing_date_err) &&
        empty($day_shed_err) && empty($injuries_err)){
        // Prepare an update statement
        $sql = "UPDATE critters SET id=?, record_date=?, name=?, sex=?, weight=?, last_fed=?,
        size_of_food=?, water_change=?, substrate_cleaned=?, blueing_date=?, day_shed=? injuries=? WHERE id=?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssssssssssss", $param_id, $param_record_date, $param_name,
            $param_sex, $param_weight, $param_last_fed, $param_size_of_food, $param_water_change, $param_substrate_cleaned,
            $param_blueing_date, $param_day_shed, $param_injuries);

            // Set parameters
            $param_id = $id;
            $param_record_date = $record_date;
            $param_name = $name;
            $param_sex = $sex;
            $param_weight = $weight;
            $param_last_fed = $last_fed;
            $param_size_of_food = $size_of_food;
            $param_water_change = $water_change;
            $param_substrate_cleaned = $substrate_cleaned;
            $param_blueing_date = $blueing_date;
            $param_day_shed = $day_shed;
            $param_injuries = $injuries;

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);

        // Prepare a select statement
        $sql = "SELECT * FROM critters WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);

            // Set parameters
            $param_id = $id;

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);

                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

                    // Retrieve individual field value
                    $id = $row["id"];
                    $record_date = $row["record_date"];
                    $name = $row["name"];
                    $sex = $row["sex"];
                    $weight = $row["weight"];
                    $last_fed = $row["last_fed"];
                    $size_of_food = $row["size_of_food"];
                    $water_change = $row["water_change"];
                    $substrate_cleaned = $row["substrate_cleaned"];
                    $blueing_date = $row["blueing_date"];
                    $day_shed = $row["day_shed"];
                    $injuries = $row["injuries"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }

            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);

        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Record</h2>
                    </div>
                    <p>Please edit the input values and submit to update the record.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group <?php echo (!empty($id_err)) ? 'has-error' : ''; ?>">
                            <label>ID</label>
                            <input type="text" name="id" class="form-control" value="<?php echo $id; ?>">
                            <span class="help-block"><?php echo $id_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($record_date_err)) ? 'has-error' : ''; ?>">
                            <label>Record Date</label>
                            <input type="text" name="record_date" class="form-control" value="<?php echo $record_date; ?>">
                            <span class="help-block"><?php echo $record_date_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                            <span class="help-block"><?php echo $name_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($sex_err)) ? 'has-error' : ''; ?>">
                            <label>Sex</label>
                            <input type="text" name="sex" class="form-control" value="<?php echo $sex; ?>">
                            <span class="help-block"><?php echo $sex_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($weight_err)) ? 'has-error' : ''; ?>">
                            <label>Weight (grams)</label>
                            <input type="text" name="weight" class="form-control" value="<?php echo $weight; ?>">
                            <span class="help-block"><?php echo $weight_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($last_fed_err)) ? 'has-error' : ''; ?>">
                            <label>Last Fed</label>
                            <input type="text" name="last_fed" class="form-control" value="<?php echo $last_fed; ?>">
                            <span class="help-block"><?php echo $last_fed_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($size_of_food_err)) ? 'has-error' : ''; ?>">
                            <label>Size of Food (grams or size)</label>
                            <input type="text" name="size_of_food" class="form-control" value="<?php echo $size_of_food; ?>">
                            <span class="help-block"><?php echo $size_of_food_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($water_change_err)) ? 'has-error' : ''; ?>">
                            <label>Water Change Date</label>
                            <input type="text" name="water_change" class="form-control" value="<?php echo $water_change; ?>">
                            <span class="help-block"><?php echo $water_change_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($substrate_cleaned_err)) ? 'has-error' : ''; ?>">
                            <label>Substrate Cleaned</label>
                            <input type="text" name="substrate_cleaned" class="form-control" value="<?php echo $substrate_cleaned; ?>">
                            <span class="help-block"><?php echo $substrate_cleaned_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($blueing_date_err)) ? 'has-error' : ''; ?>">
                            <label>Blueing Date</label>
                            <input type="text" name="blueing_date" class="form-control" value="<?php echo $blueing_date; ?>">
                            <span class="help-block"><?php echo $blueing_date_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($day_shed_err)) ? 'has-error' : ''; ?>">
                            <label>Day of Shed</label>
                            <input type="text" name="day_shed" class="form-control" value="<?php echo $day_shed; ?>">
                            <span class="help-block"><?php echo $day_shed_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($injuries_err)) ? 'has-error' : ''; ?>">
                            <label>Injuries</label>
                            <input type="text" name="injuries" class="form-control" value="<?php echo $injuries; ?>">
                            <span class="help-block"><?php echo $injuries_err;?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
